import $ from 'jquery';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel';

//import '../../../../node_modules/owl.carousel/dist/owl.carousel.js';


class OwlCarousel {

    constructor() {
        this.myFun();
    }

    myFun() {
        var myCar = document.getElementById('project-elements');
        console.log(myCar)
        $(document).ready(function () {
            myCar.owlCarousel({
                items: 3,
                autoplay: true,
                margin: 20,
                loop: true,
                nav: true,
                smartSpeed: 1000,
                autoplayHoverPause: true,
                dots: false,
                navText: ['<i class="lni-chevron-left-circle"></i>', '<i class="lni-chevron-right-circle"></i>']
            });
        });
    }
}

export default OwlCarousel;
