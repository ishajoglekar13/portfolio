$(window).on('load', function () {
    $("#preloader").delay(1500).fadeOut('slow');
});
$(document).ready(function () {
    $("#live-projects").owlCarousel({
        items: 3,

        margin: 20,
        loop: true,
        nav: true,
        smartSpeed: 1000,
        autoplayHoverPause: true,
        dots: false,
        navText: ['<i class="lni-chevron-left"></i>', '<i class="lni-chevron-right"></i>'],
        responsive: {
            0: {
                items: 1
            },
            480: {
                items: 1
            },
            768: {
                items: 2
            },
            992: {
                items: 3
            }

        }
    });
});

$(document).ready(function () {
    $("#other-projects").owlCarousel({
        items: 3,
        autoplay: true,
        margin: 20,
        loop: true,
        nav: true,
        smartSpeed: 3000,
        autoplayHoverPause: true,
        dots: false,
        navText: ['<i class="lni-chevron-left"></i>', '<i class="lni-chevron-right"></i>'],
        responsive: {
            0: {
                items: 1
            },
            480: {
                items: 1
            },
            768: {
                items: 2
            },
            992: {
                items: 3
            }

        }
    });
});

$(document).ready(function () {
    $('#my-skills-tabs').responsiveTabs({
        //    startCollapsed: 'accordion'
        animation: 'slide'
    });

});

$(document).ready(function () {
    $("#progress-elements").waypoint(function () {
        $(".progress-bar").each(function () {

            $(this).animate({
                width: $(this).attr("aria-valuenow") + "%"
            }, 800);
        });
        this.destroy();
    }, {
        offset: 'bottom-in-view'
        //        offset:'50%'
    });
});


$(document).ready(function () {
    $("#languages-that-i-speak").waypoint(function () {
        $(".progress-bar").each(function () {

            $(this).animate({
                width: $(this).attr("aria-valuenow") + "%"
            }, 800);
        });
        this.destroy();
    }, {
        offset: 'bottom-in-view'
        //        offset:'50%'
    });
});



$(function () {

    showHideNav();
    $(window).scroll(function () {
        showHideNav();
    })

    function showHideNav() {
        if ($(window).scrollTop() > 2700) {
            $('body').removeClass('white-bg');
            $('body').addClass('black-bg');
            //            $('body').addClass('color-white');
            $('.title').addClass('color-white');
            $('.subtitle').addClass('color-white');


        } else {


            $('body').removeClass('black-bg');
            $('body').addClass('white-bg');
            $('.title').removeClass('color-white');
            $('.subtitle').removeClass('color-white');


        }
    }

});


$(document).ready(function () {
    $(".project-images").owlCarousel({
        items: 2,
        margin: 20,
        loop: true,
        nav: true,
        smartSpeed: 800,
        autoplayHoverPause: true,
        autoplay: true,
        dots: true,

        responsive: {
            0: {
                items: 1
            },
            480: {
                items: 1
            },
            768: {
                items: 2
            },
            992: {
                items: 2
            }

        }
    });
});

$(document).ready((function () {
    var c = $('#submitbtn');
    $("#submitbtn").on("click", function (event) {
        event.preventDefault();
        $("#contact-form span").remove();
        $("#contact-form input[type='text']").css("border-right", "0px");
        $("#contact-form textarea").css("border-right", "0px");
        // Test fullname
        var fullname = $("#contact-form input[name='name']");
        if (fullname.val() === "") {
            fullname.after("<span>This field must be filled</span>").css("border-right", "3px solid #a94442");
        }
        // Test email
        var email = $("#contact-form input[name='email']");
        var patt = /^[a-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$/;
        if (!patt.test(email.val())) {
            email.after("<span>Invalid Email</span>").css("border-right", "3px solid #a94442");
        }
        // Test message
        var txtarea = $("#contact-form textarea");
        if (txtarea.val() === "") {
            txtarea.after("<span>This field must be filled</span>").css("border-right", "3px solid #a94442");
        }

        $("#contact-form  input[name='email']").on("keyup", function () {
            var patt = /^[a-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$/;
            if (patt.test($(this).val())) {
                $(this).css("border-right", "0px").next("span").remove();
            }
            return false;
        });

        // Remove fullname error
        $("#contact-form  input[name='name']").on("keyup", function () {
            if ($(this).val() !== "") {
                $(this).css("border-right", "0px").next("span").remove();
            }
            return false;
        });

        // Remove textarea error
        $("#contact-form  textarea").on("keyup", function () {
            if ($(this).val() !== "") {
                $(this).css("border-right", "0px").next("span").remove();
            }
            return false;
        });
        // Comment Form Errors
        $("#contact-form input[type='button']").on("click", function () {

            $("#contact-form input[type='text']").css("border-color", "#FF0000");
            $("#contact-form textarea").css("border-color", "#FF0000");

            // Test fullname input
            var fullname = $("#contact-form input[name='name']");
            if (fullname.val() !== "") {
                fullname.css("border-color", "#EEEEEE");
            }

            // Test message input
            var txtarea = $("#contact-form textarea");
            if (txtarea.val() !== "") {
                txtarea.css("border-color", "#EEEEEE");
            }

            return false;
        });

        // Remove Errors
        $("#contact-form input[name='name']").on("keyup", function () {
            if ($(this).val() !== "") {
                $(this).css("border-color", "#EEEEEE");
            }

            return false;
        });

        $("#contact-form textarea").on("keyup", function () {
            if ($(this).val() !== "") {
                $(this).css("border-color", "#EEEEEE");
            }

            return false;
        });

        if ($("#contact-form span").length === 0) {
            $.ajax({
                url: 'https://formspree.io/xayprlzl',
                method: 'POST',
                data: {
                    message: $('form').serialize()
                },
                dataType: 'json'
            }).done(function (data) {
                $('#contact-form')
                    .find('input[type=text], input[type=email], textarea')
                    .val('');
                if ($("#contact-form span").length === 0) {
                    $("#exampleModalCenter").modal("show");
                }
            });
        }
    });
}));


new WOW().init();
