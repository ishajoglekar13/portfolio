import $ from 'jquery';
import smoothScroll from 'jquery-smooth-scroll';

class SmoothScroll {
    constructor() {
        this.headerLinks = $(".smooth-scroll a");
        this.addSmoothScrolling();
        console.log("called");
    }
    addSmoothScrolling() {
        this.headerLinks.smoothScroll({
            offset: 200;
        });
    }

}
export default SmoothScroll;
