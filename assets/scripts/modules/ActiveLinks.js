import $ from 'jquery';
import waypoints from "../../../../node_modules/waypoints/lib/noframework.waypoints";
class ActiveLinks {
    constructor() {
        this.pageSections = $(".section");
        this.headerLinks = $(".menu-list a");
        this.createWaypoints();
        this.navbarDesign();

    }
    createWaypoints() {
        var that = this;
        this.pageSections.each(function () {
            var currentPageSection = this;
            new Waypoint({
                element: currentPageSection,
                handler: function (direction) {
                    if (direction == "down") {
                        var linkElement = currentPageSection.getAttribute('data-active-link');
                        that.headerLinks.removeClass('active');
                        $(linkElement).addClass("active");
                    } else {

                    }
                },
                offset: "40%"
            });
            new Waypoint({
                element: currentPageSection,
                handler: function (direction) {
                    if (direction == "up") {
                        var linkElement = currentPageSection.getAttribute('data-active-link');
                        that.headerLinks.removeClass('active');
                        $(linkElement).addClass("active");
                    } else {

                    }
                },
                offset: "-60%"
            });
        });
    }


    navbarDesign() {
        $(function () {

            showHideNav();
            $(window).scroll(function () {
                showHideNav();
            })

            function showHideNav() {
                if ($(window).scrollTop() > 50) {
                    $('header').addClass('yellow-nav-top');
                    $("#back-to-top").fadeIn();


                } else {


                    $('header').removeClass('yellow-nav-top');
                    $("#back-to-top").fadeOut();
                }
            }

        });
    }

}
export default ActiveLinks;
