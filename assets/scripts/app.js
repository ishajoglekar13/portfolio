import $ from 'jquery';
import "../styles/style.css";
import RevealOnScroll from "./modules/RevealOnScroll";
import OwlCarousel from "./modules/OwlCarousel";
import SS from "./modules/SS";
import MobileMenu from "./modules/MobileMenu";
import ActiveLinks from "./modules/ActiveLinks";
import './modules/temp';

import 'lazysizes';

if (module.hot) {
    module.hot.accept();
}
let mobileMenu = new MobileMenu();
new RevealOnScroll($(".section"), "60%");
new SS();
new ActiveLinks();
